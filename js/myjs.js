$(document).ready(function() { // document ready
    $('#calendar').fullCalendar({
        defaultView: 'agendaWeek',
        defaultDate: '2018-04-07',
        timezone: 'Asia/Ho_Chi_Minh',
        height: 600,
        columnHeaderFormat :'ddd D/M',
        editable: true,
        selectable: true,
        allDay: false,
        droppable: true,
        navLinks: true,
        eventColor: '#378006',
        weekNumbers: true,
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        // timeFormat: 'H(:mm)',
        weekNumbersWithinDays: true,
        eventLimit: true, // allow "more" link when too many events
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaDay,agendaTwoDay,agendaWeek,month,listWeek'
        },
        titleFormat: 'DD/MM/YYYY',
        views: {
            agendaTwoDay: {
                type: 'agenda',
                duration: { days: 4 },
                // views that are more than a day will NOT do this behavior by default
                // so, we need to explicitly enable it
                groupByResource: true
                //// uncomment this line to group by day FIRST with resources underneath
                //groupByDateAndResource: true
            }
        },
        //// uncomment this line to hide the all-day slot
        //allDaySlot: false,
        // resources: [
        //     { id: 'a', title: 'Room A' },
        //     { id: 'b', title: 'Room B', eventColor: 'green' },
        //     { id: 'c', title: 'Room C', eventColor: 'orange' },
        //     { id: 'd', title: 'Room D', eventColor: 'red' }
        // ],
        events: [
            { id: '1',  start: '2018-04-06', end: '2018-04-08', title: 'event 1', allDay: true,color: 'red' },
            { id: '2',  start: '2018-04-07T09:00:00', end: '2018-04-07T14:00:00', title: 'event 2',color: 'red' },
            { id: '3',  start: '2018-04-07T12:00:00', end: '2018-04-08T06:00:00', title: 'event 3',color: 'blue' },
            { id: '4',  start: '2018-04-07T07:30:00', end: '2018-04-07T09:30:00', title: 'event 4',color: 'orange' },
            { id: '5',  start: '2018-04-07T10:00:00', end: '2018-04-07T15:00:00', title: 'event 5',color: 'green' }
        ],
        select: function(start, end) { /*function click space*/
            starttime = start.format('DD/MM/YYYY HH:mm');
            endtime = end.format('DD/MM/YYYY HH:mm');
            var mywhen = starttime + ' - ' + endtime;
            var allDay = !start.hasTime() && !end.hasTime();
            console.log(start);
            $('#myModal #patientName').val("");
            $('#myModal #submitButton').attr('data-action', 'renderEvent');
            $('#myModal #datetime_from').val(starttime);
            $('#myModal #datetime_to').val(endtime);
            $('#myModal #apptStartTime').val(start);
            $('#myModal #apptEndTime').val(end);
            $('#myModal #apptAllDay').val(allDay);
            $('#myModal #when').text(mywhen);
            $('#myModal').modal('show');
            $('#deleteEvent').addClass('hide');
        },
        // dayClick: function(date,allDay ,jsEvent, view, resource) {
        // console.log(
        // jsEvent,
        // date.format(),
        // );
        // },
        eventClick: function(calEvent, jsEvent, view) { /*function when click event*/
            $('#deleteEvent').removeClass('hide');
            starttime = calEvent.start.format('DD/MM/YYYY HH:mm');
            endtime = calEvent.end.format('DD/MM/YYYY HH:mm');
            var mywhen = starttime + ' - ' + endtime;
            var event_name = calEvent.title;
            $('#myModal #datetime_from').val(starttime);
            $('#myModal #datetime_to').val(endtime);
            $('#myModal #apptStartTime').val(calEvent.start);
            $('#myModal #apptEndTime').val(calEvent.end);
            $('#myModal #idEvent').val(calEvent.id);
            // $('#myModal #apptAllDay').val(allDay);
            $('#myModal #when').text(mywhen);
            $('#myModal #patientName').val(event_name);
            $('#myModal #submitButton').attr('data-action', 'updateEvent');
            $('#myModal').modal('show');

            console.log(calEvent);
            // change the border color just for fun
            // $(this).css('border-color', 'red');
        },
        windowResize: function(view) {
            console.log('The calendar has adjusted to a window resize');
        },
        eventResize: function(event, delta, revertFunc) {
            console.log(event.title + " end is now " + event.end.format());
        },
        drop: function(date) {
            console.log("Dropped on " + date.format());
        },
        eventDrop: function(event, delta, revertFunc) { /*function drop event*/
            console.log(event.title + " was dropped on " + event.start.format());
        },
        navLinkWeekClick: function(weekStart, jsEvent) {
            console.log('week start', weekStart.format()); // weekStart is a moment
            console.log('coords', jsEvent.pageX, jsEvent.pageY);
        }
    });
    // onClick button save event
    $('#submitButton').on('click', function(e) {
        e.preventDefault();
        var data = {
            id: Math.floor(Math.random() * 100),
            title: $('#patientName').val(),
            start: $('#apptStartTime').val(),
            end: $('#apptEndTime').val(),
            allDay: ($('#apptAllDay').val() == "true"),
        };
        doSubmit(data);
    });
    // onclick submit edit Event
    $('#saveEvent').on('click', function(e) {
        e.preventDefault();
        if ($('#eventAllDay').val() == 'true') {
            isAllday = true;
        } else {
            isAllday = false
        };
        var data = {
            id: Math.floor(Math.random() * 100),
            title: $('#eventTitle').val(),
            start: $('#date_from').val() + ' ' + $('#time_from').val(),
            end: $('#date_to').val() + ' ' + $('#time_to').val(),
            allDay: isAllday
        };
        doSubmit(data);
    });
    // delete event
    $('#deleteEvent').on('click', function(e) {
        var idEvent = $('#idEvent').val();
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    if (idEvent) {
                        $("#calendar").fullCalendar('removeEvents', idEvent);
                        $("#myModal").modal('hide');
                    }
                }
            });
    });
    // function submit
    function doSubmit(data) {
        var action = $('#submitButton').attr('data-action');
        var idEvent = $('#idEvent').val();
        console.log(data);
        $("#myModal").modal('hide');
        $("#detailEvent").modal('hide');
        if (action != 'renderEvent') {
            $("#calendar").fullCalendar('removeEvents', idEvent);
        }
        $("#calendar").fullCalendar('renderEvent', data,
            true);
    };
    // Check allDay
    $('#allDay').change(function() {
        var checked = $('#allDay').is(":checked");
        if (checked) {
            $('#time_from').val("00:00");
            $('#time_to').val("00:00");
            $('.time_to').addClass('hide');
            $('.time_from').addClass('hide');
            $('#eventAllDay').val(checked)
        } else {
            $('.time_to').removeClass('hide');
            $('.time_from').removeClass('hide');
            $('#eventAllDay').val(checked)
        }
    });
    
    //Clear data when show modal
    $('#detailEvent').on('shown.bs.modal', function() {
        $('#eventTitle').val("");
        $('#date_from').val("");
        $('#time_from').val("");
        $('#date_to').val("");
        $('#time_to').val("");
        $('.time_to').removeClass('hide');
        $('.time_from').removeClass('hide');
        $("#allDay").prop("checked", false);
    });
    // datetimepicker
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
        showMeridian: 1
    });
    // datepicker
    $('.form_date').datetimepicker({
        language: 'en',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    // timepicker
    $('.form_time').datetimepicker({
        language: 'en',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });
});

// AIzaSyBrFRMtjUMDgZhNalHwwDAwYXjFXuzbNaE